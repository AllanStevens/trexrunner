package com.nellyfish.trexrunner;

import android.content.res.Configuration;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.webkit.WebView;


public class MainActivity extends ActionBarActivity {

    private Menu menu;
    private WebView mWebView;
    private String currentUrl = "file:///android_asset/trex.htm";
    //private int orientation = getScreenOrientation();

    @Override
    public void onBackPressed() {

        if (getSupportActionBar().isShowing()) {
            super.onBackPressed();

        }
        toggleFullscreen(false);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        startWebview();
    }

    private void toggleFullscreen(boolean b) {

        if (b) {

           /*getSupportActionBar().hide();
            getWindow().setFlags(
                    WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN); */
            // Set the IMMERSIVE flag.
            // Set the content to appear under the system bars so that the content
            // doesn't resize when the system bars hide and show.
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                            | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                            | View.SYSTEM_UI_FLAG_IMMERSIVE);

        }

        else{

           /* getSupportActionBar().show();
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN); */

            // This snippet shows the system bars. It does this by removing all the flags
            // except for the ones that make the content appear under the system bars.

            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);

        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        // go fullscreen if horizontal
        if(getScreenOrientation() == Configuration.ORIENTATION_LANDSCAPE) {
            toggleFullscreen(true);
        }
        else {
            toggleFullscreen(false);
        }
    }

    protected void startWebview() {


        // Initialize the WebView if necessary
        if (mWebView == null)
        {
            mWebView = (WebView) findViewById(R.id.webView);

            // Enable Javascript
            WebSettings webSettings = mWebView.getSettings();
            webSettings.setJavaScriptEnabled(true);

            mWebView.loadUrl(currentUrl);
        }
    }

    public int getScreenOrientation()
    {
        Display getOrient = getWindowManager().getDefaultDisplay();
        int orientation = Configuration.ORIENTATION_UNDEFINED;
        if(getOrient.getWidth()==getOrient.getHeight()){
            orientation = Configuration.ORIENTATION_SQUARE;
        } else{
            if(getOrient.getWidth() < getOrient.getHeight()){
                orientation = Configuration.ORIENTATION_PORTRAIT;
            }else {
                orientation = Configuration.ORIENTATION_LANDSCAPE;
            }
        }
        return orientation;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        switch (item.getItemId())
        {
            case R.id.action_fullscreen:
                toggleFullscreen(true);
                return true;

            case R.id.action_difficulty:

                if ( item.getTitle().equals("Impossible Version") )
                {
                    currentUrl = "file:///android_asset/trex_hard.htm";
                    item.setTitle("Standard Version");
                    menu.findItem(R.id.action_night).setTitle("Night Version");
                }
                else
                {
                    currentUrl = "file:///android_asset/trex.htm";
                    item.setTitle("Impossible Version");
                }
                break;

            case R.id.action_night:
                if ( item.getTitle().equals("Night Version") )
                {
                    currentUrl = "file:///android_asset/trex_night.htm";
                    item.setTitle("Standard Version");
                    menu.findItem(R.id.action_difficulty).setTitle("Impossible Version");

                }
                else
                {
                    currentUrl = "file:///android_asset/trex.htm";
                    item.setTitle("Night Version");
                }
                break;

            case R.id.action_about:
                currentUrl = "file:///android_asset/about.htm";
                break;
        }

        mWebView.loadUrl(currentUrl);

        return true;

        //return super.onOptionsItemSelected(item);
    }
}

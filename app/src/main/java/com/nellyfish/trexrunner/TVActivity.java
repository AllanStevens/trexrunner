package com.nellyfish.trexrunner;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Window;
import android.webkit.WebSettings;
import android.webkit.WebView;


public class TVActivity extends ActionBarActivity {

    private WebView mWebView;
    private String currentUrl = "file:///android_asset/trex_tv.htm";
    //private int orientation = getScreenOrientation();

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tv);

        // Initialize the UI
        startWebview();
    }

    protected void startWebview() {

        // Initialize the WebView if necessary
        if (mWebView == null)
        {
            mWebView = (WebView) findViewById(R.id.webView);

            // Enable Javascript
            WebSettings webSettings = mWebView.getSettings();
            webSettings.setJavaScriptEnabled(true);

            mWebView.loadUrl(currentUrl);
        }
    }

}
